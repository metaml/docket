VER = 2.0.0
URL = http://d3kbcqa49mib13.cloudfront.net/spark-${VER}-bin-hadoop2.7.tgz
TGZ = $(shell echo ${URL} | awk -F/ '{print $$NF}')
DIR = $(shell echo ${TGZ} | sed 's/\.tgz$$//g')

spark: ## install spark
	[ -d /opt ] || sudo mkdir -p /opt
	if [ ! -f /opt/$@/bin ]; then \
		cd /var/tmp \
		&& rm -f ${TGZ} \
		&& wget ${URL} \
		&& tar xvf ${TGZ} \
		&& mv -f /var/tmp/${DIR} /opt/$@ \
		&& rm -f /opt/spark/bin/*.cmd \
		&& echo installed $@; \
	fi

info:
	echo ${TGZ}
	echo ${DIR}
